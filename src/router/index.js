import Vue from 'vue'
import Router from 'vue-router'
//import CartPage from '../pages/CartPage'
import HomePage from '../pages/HomePage.vue'
import Catalog from '../pages/Catalog.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomePage
    },
    {
      path: '/catalog',
      name: 'catalog',
      component: Catalog
    },
    /*
    {
      path: '/product/:id',
      name: 'product',
      component: ProductPage
    }
    */
  ]
})
